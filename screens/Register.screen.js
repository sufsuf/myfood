import React, { useState } from 'react';
import {
  StyleSheet, Text, View, TextInput, Image, TouchableHighlight, Alert,
} from 'react-native';

import { Actions } from 'react-native-router-flux';

import { connect } from 'react-redux';
import { GET_REGISTRATION } from '../redux/store/actions/actionsTypes';

import TopBar from '../components/TopBar.component';
import {
  colors, appProps, font,
} from '../constants/Env';

const userIcon = require('../assets/imgs/icon-user.png');
const lockIcon = require('../assets/imgs/icon-lock.png');

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.white,
    alignItems: 'center',
    justifyContent: 'center',
    width: '100%',
    height: '100%',
  },
  subContainer: {
    width: '100%',
    height: '88%',
    alignItems: 'center',
    paddingTop: '50%',
  },
  inputContainer: {
    borderWidth: 1,
    borderColor: colors.orange,
    borderRadius: appProps.borderRadius,
    width: '75%',
    height: 40,
    alignItems: 'center',
    paddingLeft: appProps.textInputPadding,
    flexDirection: 'row',
    marginTop: 10,
    marginBottom: 10,
  },
  textInput: {
    marginLeft: 10,
    fontFamily: font.regular,
    fontSize: font.textSize,
    width: '75%',
    height: '80%',
    paddingTop: 3,
  },
  buttonContainer: {
    backgroundColor: colors.orange,
    borderRadius: appProps.borderRadius,
    alignItems: 'center',
    justifyContent: 'center',
    width: '75%',
    height: 40,
    marginTop: 30,
  },
  buttonText: {
    color: colors.white,
    fontFamily: font.bold,
    fontSize: font.textSize,
  },
  littleTextContainer: {
    marginTop: 15,
  },
  littleText: {
    color: colors.gray,
    fontFamily: font.regular,
    fontSize: font.littleTextSize,
    textDecorationLine: 'underline',
  },
  rowContainer: {
    flexDirection: 'row',
  },
});

const RegisterScreen = (props) => {
  const { dispatch } = props;
  const [pseudo, setPseudo] = useState('LDS1');
  const [password, setPassword] = useState('ouioui');
  const [firstname, setFirstname] = useState('Antoine');
  const [lastname, setLastname] = useState('Yvroud');
  const [passwordValid, setPasswordValid] = useState('ouioui');

  const handleLoginSubmit = async () => {
    if ((pseudo.length < 1 || firstname.length < 1 || lastname.length < 1)
      || (password !== passwordValid)) {
      Alert.alert('Informations invalides ou mots de passes non correspondant');
    } else {
      const connectionData = {
        user: {
          pseudo, firstname, lastname, password,
        },
      };
      const action = { type: GET_REGISTRATION, value: connectionData };
      await dispatch(action);
      Actions.HomeScreen();
    }
  };

  return (
    <View style={styles.container}>
      <TopBar from="login" />
      <View style={styles.subContainer}>
        <View style={styles.inputContainer}>
          <Image source={userIcon} />
          <TextInput
            style={styles.textInput}
            autoCapitalize="none"
            onChangeText={(newFirstname) => setFirstname(newFirstname)}
            value={firstname}
            returnKeyType="next"
            placeholder="Prenom"
            blurOnSubmit={false}
            placeholderTextColor={colors.gray}
          />
        </View>
        <View style={styles.inputContainer}>
          <Image source={userIcon} />
          <TextInput
            style={styles.textInput}
            autoCapitalize="none"
            onChangeText={(newLastname) => setLastname(newLastname)}
            value={lastname}
            placeholder="Nom de famille"
            placeholderTextColor={colors.gray}
          />
        </View>
        <View style={styles.inputContainer}>
          <Image source={userIcon} />
          <TextInput
            style={styles.textInput}
            autoCapitalize="none"
            onChangeText={(newPseudo) => setPseudo(newPseudo)}
            value={pseudo}
            returnKeyType="next"
            placeholder="Pseudo"
            blurOnSubmit={false}
            placeholderTextColor={colors.gray}
          />
        </View>
        <View style={styles.inputContainer}>
          <Image source={lockIcon} />
          <TextInput
            style={styles.textInput}
            secureTextEntry
            autoCapitalize="none"
            onChangeText={(newPassword) => setPassword(newPassword)}
            value={password}
            placeholder="Mot de passe"
            placeholderTextColor={colors.gray}
          />
        </View>
        <View style={styles.inputContainer}>
          <Image source={lockIcon} />
          <TextInput
            style={styles.textInput}
            secureTextEntry
            autoCapitalize="none"
            onChangeText={(newPasswordValid) => setPasswordValid(newPasswordValid)}
            value={passwordValid}
            returnKeyType="done"
            placeholder="Confimer mot de passe"
            onSubmitEditing={handleLoginSubmit}
            placeholderTextColor={colors.gray}
          />
        </View>
        <TouchableHighlight onPress={handleLoginSubmit} style={styles.buttonContainer} underlayColor="#FFF">
          <View>
            <Text style={styles.buttonText}>S&apos;inscrire</Text>
          </View>
        </TouchableHighlight>
        <TouchableHighlight onPress={() => Actions.pop()} style={styles.littleTextContainer} underlayColor="#FFF">
          <View style={styles.littleTextContainer}>
            <Text style={styles.littleText}>Tu as un compte ? Connecte toi ici !</Text>
          </View>
        </TouchableHighlight>
      </View>
    </View>
  );
};

const mapStateToProps = (state) => ({
  username: state.profile,
});

export default connect(mapStateToProps)(RegisterScreen);
