import React, { useState } from 'react';
import {
  StyleSheet, Text, View, TextInput, Image, TouchableHighlight, Alert, ScrollView,
} from 'react-native';

import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';

import axios from 'axios';

import { GET_SEARCH_USER } from '../redux/store/actions/actionsTypes';
import {
  colors, font, apiUrl,
} from '../constants/Env';
import TopBar from '../components/TopBar.component';
import Footer from '../components/Footer.component';

const searchIcon = require('../assets/imgs/icon-search.png');
const goIcon = require('../assets/imgs/icon-go.png');

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.white,
    alignItems: 'center',
    justifyContent: 'center',
    width: '100%',
    height: '100%',
  },
  subContainer: {
    width: '90%',
    height: '78%',
    alignItems: 'center',
    paddingTop: '5%',
  },
  topBarContainer: {
    width: '100%',
    height: '10%',
  },
  footerContainer: {
    width: '100%',
    height: '10%',
  },
  searchContainer: {
    flexDirection: 'row',
    height: 50,
    width: '100%',
    borderWidth: 1,
    borderColor: colors.gray,
    borderRadius: 25,
    padding: 10,
    paddingLeft: 20,
    alignItems: 'center',
  },
  searchResultContainer: {
    width: '100%',
    height: '86%',
    marginTop: 40,
  },
  textInput: {
    marginLeft: 20,
    fontFamily: font.regular,
    fontSize: font.textSize,
    width: '75%',
    height: '80%',
    paddingTop: 3,
  },
  cellContainer: {
    backgroundColor: colors.orange,
    marginBottom: 10,
    height: 70,
    justifyContent: 'center',
    flexDirection: 'row',
  },
  classicText: {
    color: colors.orange,
    fontFamily: font.regular,
    fontSize: font.textSize,
  },
  scrollView: {
    width: '100%',
    height: '100%',
    borderTopWidth: 2,
    borderColor: colors.gray,
    flex: 1,
  },
});

const UserCell = (props) => {
  const { user } = props;
  return (
    <TouchableHighlight onPress={() => Actions.MyPageScreen({ user })} underlayColor="#FFF">
      <View style={styles.cellContainer}>
        <View style={{ justifyContent: 'center', width: '15%', alignItems: 'center' }}>
          <Image
            source={{ uri: 'https://www.pngitem.com/pimgs/m/421-4212617_person-placeholder-image-transparent-hd-png-download.png' }}
            style={{
              width: 40, height: 40, borderRadius: 20, borderColor: colors.white, borderWidth: 2,
            }}
          />
        </View>
        <View style={{ justifyContent: 'center', width: '70%' }}>
          <Text
            style={[styles.classicText, { color: colors.white, marginLeft: 15 }]}
          >
            {user.pseudo}
          </Text>
        </View>
        <View style={{ justifyContent: 'center', width: '15%' }}>
          <Image
            source={goIcon}
            style={{ width: 30, height: 30 }}
          />
        </View>
      </View>
    </TouchableHighlight>
  );
};

const SearchScreen = (props) => {
  const [search, setSearch] = useState('LDS');
  const [results, setResults] = useState([]);
  const [noResults, setNoResults] = useState(false);

  const handleSearchSubmit = async () => {
    const searchData = { search };
    const action = { type: GET_SEARCH_USER, value: searchData };
    await props.dispatch(action);
    axios.get(`http://${apiUrl}:8888/user/search`, {
      params: {
        pseudo: search,
      },
    })
      .then((response) => {
        if (response.data.length === 0) {
          setNoResults(true);
        } else {
          setNoResults(false);
        }
        setResults(response.data);
      })
      .catch((error) => {
        if (error.response.status >= 400 && error.response.status < 500) {
          Alert.alert('Identifiants incorrects');
        }
      });
  };
  return (
    <View style={styles.container}>
      <TopBar from="app" />
      <View style={styles.subContainer}>
        <View style={styles.searchContainer}>
          <Image source={searchIcon} />
          <TextInput
            style={styles.textInput}
            autoCapitalize="none"
            onChangeText={(newSearch) => setSearch(newSearch)}
            value={search}
            returnKeyType="search"
            placeholder="Rechercher un chef"
            blurOnSubmit={false}
            placeholderTextColor={colors.gray}
            onSubmitEditing={handleSearchSubmit}
          />
        </View>
        <View style={styles.searchResultContainer}>
          <ScrollView
            contentContainerStyle={{ flexGrow: 1 }}
            style={styles.scrollView}
          >
            { results.length > 0 && results.map((user) => (
              <UserCell key={user.id} user={user} />
            ))}
            { noResults && (
              <Text>
                Pas de resultat pour &apos;
                {search}
                &apos;
              </Text>
            )}
          </ScrollView>
        </View>
      </View>
      <View style={styles.footerContainer}>
        <Footer currentScreen="Search" />
      </View>
    </View>
  );
};

const mapStateToProps = (state) => ({
  profile: state.profile,
});

export default connect(mapStateToProps)(SearchScreen);
