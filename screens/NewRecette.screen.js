import React, { useEffect, useState } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  Image,
  TouchableHighlight,
  Alert,
  Linking,
  ScrollView,
  Platform,
} from 'react-native';
import { connect } from 'react-redux';
import axios from 'axios';
import * as Permissions from 'expo-permissions';
import {
  colors, appProps, font, apiUrl,
} from '../constants/Env';
import TopBar from '../components/TopBar.component';
import { GET_CONNECTION } from '../redux/store/actions/actionsTypes';
import CameraScreen from './Camera.screen';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.white,
    alignItems: 'center',
    justifyContent: 'center',
    width: '100%',
    height: '100%',
  },
  subContainer: {
    width: '100%',
    height: '88%',
    alignItems: 'center',
    paddingTop: 30,
  },
  inputContainer: {
    borderWidth: 1,
    borderColor: colors.orange,
    borderRadius: appProps.borderRadius,
    width: '75%',
    height: 30,
    alignItems: 'center',
    paddingLeft: appProps.textInputPadding,
    flexDirection: 'row',
    marginTop: 10,
    marginBottom: 10,
  },
  textInput: {
    marginLeft: 10,
    fontFamily: font.regular,
    fontSize: font.textSize,
    width: '94%',
    paddingTop: 10,
  },
  titleContainer: {
    // backgroundColor: 'red',
    width: '85%',
    marginTop: 20,
    marginBottom: 40,
    borderColor: colors.orange,
    borderWidth: 1,
    borderRadius: 10,
    paddingTop: 10,
  },
  contentContainer: {
    height: '50%',
    width: '85%',
    marginTop: 20,
    marginBottom: 40,
    borderColor: colors.orange,
    borderWidth: 1,
    borderRadius: 10,
    paddingTop: 10,
  },
  classicText: {
    color: colors.orange,
    fontFamily: font.regular,
    fontSize: font.textSize,
    marginTop: '-2%',
  },
  buttonContainer: {
    backgroundColor: colors.orange,
    borderRadius: appProps.borderRadius,
    alignItems: 'center',
    justifyContent: 'center',
    width: '55%',
    height: 40,
    marginTop: 30,
  },
  buttonText: {
    color: colors.white,
    fontFamily: font.bold,
    fontSize: font.textSize,
  },
});

const NewRecetteScreen = (props) => {
  const { profile, dispatch } = props;
  const [title, setTitle] = useState('Pizza chanme 🍕');
  const [content, setContent] = useState('contenu de recette de pizza chanme');
  const [hasPermissions, setHasPermissions] = useState(null);
  const [isCameraView, setIsCameraView] = useState(false);

  const state = profile;
  const [capturedImages, setCapturedImages] = useState([]);

  const checkPermissions = async () => {
    if (Platform.OS === 'ios') {
      Permissions.askAsync(Permissions.MEDIA_LIBRARY).then((r) => {
        if (r.status !== 'granted') {
          Alert.alert(
            'Accès album non autorisé',
            'Veuillez autoriser l\'application à accéder à votre album pour pouvoir choisir des photos',
            [
              { text: 'Annuler', onPress: () => Alert.alert('Accès non donné') },
              { text: 'Autoriser', onPress: () => Linking.openURL('app-settings:') },
            ],
            { cancelable: false },
          );
        }
      });
    }
    let { status: existingCameraStatus } = await Permissions.getAsync(Permissions.CAMERA);
    if (existingCameraStatus !== 'granted') {
      Permissions.askAsync(Permissions.CAMERA).then((result) => {
        setHasPermissions(result.status === 'granted');
        existingCameraStatus = result.status;
      });
    }
    if (existingCameraStatus !== 'granted') {
      Alert.alert(
        'Accès caméra non autorisé',
        'Veuillez autoriser l\'application à accéder à votre caméra pour pouvoir prendre des photos',
        [
          { text: 'Annuler', onPress: () => Alert.alert('Demande annulée') },
          { text: 'Autoriser', onPress: () => Linking.openURL('app-settings:') },
        ],
        { cancelable: false },
      );
    }
    return existingCameraStatus;
  };

  useEffect(() => {
    checkPermissions().then((r) => {
      setHasPermissions(r === 'granted');
    });
  }, []);

  const handleSubmitNewRecette = () => {
    axios.post(`http://${apiUrl}:8888/recette`, {
      title,
      content,
      userId: state.user._id,
    })
      .then(() => {
        axios.get(`http://${apiUrl}:8888/profile`, {
          params: {
            id: state.user._id,
          },
        })
          .then((response) => {
            Alert.alert('Ta recette est en ligne !');
            const connectionData = { user: response.data };
            const action = { type: GET_CONNECTION, value: connectionData };
            dispatch(action);
          })
          .catch((error) => {
            if (error.response.status >= 400 && error.response.status < 500) {
              Alert.alert('Incident user');
            }
          });
      })
      .catch((error) => {
        if (error.response.status >= 400 && error.response.status < 500) {
          Alert.alert('Incident');
        }
      });
  };

  const handleCameraAccess = () => {
    if (hasPermissions === false) {
      Alert.alert('MyFood n\'a pas accès à la caméra :|');
    } else if (hasPermissions === true) {
      setIsCameraView(true);
    }
  };

  return isCameraView ? (
    <CameraScreen setCapturedImages={setCapturedImages} setIsCameraView={setIsCameraView} />
  ) : (
    <View style={styles.container}>
      <TopBar from="newrecette" />
      <View style={styles.subContainer}>
        <View style={styles.titleContainer}>
          <View style={{
            backgroundColor: 'white', marginTop: '-5%', height: 10, marginLeft: 20, width: '50%', paddingLeft: 10,
          }}
          >
            <Text style={styles.classicText}>Nom de la recette</Text>
          </View>
          <TextInput
            style={[styles.textInput, { height: 50 }]}
            autoCapitalize="none"
            onChangeText={(newTitle) => setTitle(newTitle)}
            value={title}
            returnKeyType="done"
            placeholder="Titre de la recette"
            onSubmitEditing={handleSubmitNewRecette}
            placeholderTextColor={colors.gray}
          />
        </View>
        <View style={styles.contentContainer}>
          <View style={{
            backgroundColor: 'white', marginTop: '-5%', height: 10, marginLeft: 20, width: '55%', paddingLeft: 10,
          }}
          >
            <Text style={styles.classicText}>Contenu de la recette</Text>
          </View>
          <TextInput
            multiline
            style={styles.textInput}
            autoCapitalize="none"
            onChangeText={(newContent) => setContent(newContent)}
            value={content}
            returnKeyType="done"
            placeholder="Contenu de la recette"
            onSubmitEditing={handleSubmitNewRecette}
            placeholderTextColor={colors.gray}
          />
        </View>
        <ScrollView style={{ flex: 1 }} contentContainerStyle={{ width: '100%', flex: 1, flexDirection: 'row' }}>
          {capturedImages && capturedImages.length > 0 && capturedImages.map((image) => <Image key={image.uri} source={{ uri: image.uri }} style={{ marginRight: 5, width: '25%', height: '100%' }} />)}
        </ScrollView>
        <TouchableHighlight onPress={handleCameraAccess} style={styles.buttonContainer} underlayColor="#FFF">
          <View>
            <Text style={styles.buttonText}>Prendre une Photo</Text>
          </View>
        </TouchableHighlight>
        <TouchableHighlight onPress={handleSubmitNewRecette} style={styles.buttonContainer} underlayColor="#FFF">
          <View>
            <Text style={styles.buttonText}>Mettre en ligne</Text>
          </View>
        </TouchableHighlight>
      </View>
    </View>
  );
};

const mapStateToProps = (state) => ({
  profile: state.profile,
});

export default connect(mapStateToProps)(NewRecetteScreen);
