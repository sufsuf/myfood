import React, { useRef, useState } from 'react';
import { Camera } from 'expo-camera';
import { TouchableOpacity, View } from 'react-native';
import { FontAwesome, MaterialCommunityIcons } from '@expo/vector-icons';
import * as ImagePicker from 'expo-image-picker';

const CameraScreen = (props) => {
  const { setCapturedImages, setIsCameraView } = props;
  const [cameraType, setCameraType] = useState(Camera.Constants.Type.back);

  const camera = useRef(null);

  const handleCameraType = () => {
    setCameraType(cameraType === Camera.Constants.Type.back
      ? Camera.Constants.Type.front
      : Camera.Constants.Type.back);
  };

  const takePicture = async () => {
    if (camera.current) {
      camera.current.takePictureAsync().then((r) => {
        setCapturedImages((old) => [...old, r]);
        // setPreviewVisible(true);
        // TODO Upload du result
      }).catch(() => {
      });
    }
  };

  const pickImage = async () => {
    const image = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.Images,
      allowsEditing: true,
      aspect: [3, 3],
      quality: 1,
      base64: true,
    });
    setCapturedImages((old) => [...old, image]);
    // TODO Upload du result
  };

  return (
    <View style={{ flex: 1 }}>
      <Camera style={{ flex: 1 }} type={cameraType} ref={camera}>
        <View style={{
          flex: 1, flexDirection: 'row', justifyContent: 'space-between', margin: 20,
        }}
        >
          <TouchableOpacity
            style={{
              alignSelf: 'flex-end',
              alignItems: 'center',
              backgroundColor: 'transparent',
            }}
            onPress={() => setIsCameraView(false)}
          >
            <FontAwesome
              name="arrow-left"
              style={{ color: '#fff', fontSize: 40 }}
            />
          </TouchableOpacity>
          <TouchableOpacity
            style={{
              alignSelf: 'flex-end',
              alignItems: 'center',
              backgroundColor: 'transparent',
            }}
            onPress={pickImage}
          >
            <FontAwesome
              name="image"
              style={{ color: '#fff', fontSize: 40 }}
            />
          </TouchableOpacity>
          <TouchableOpacity
            style={{
              alignSelf: 'flex-end',
              alignItems: 'center',
              backgroundColor: 'transparent',
            }}
            onPress={takePicture}
          >
            <FontAwesome
              name="camera"
              style={{ color: '#fff', fontSize: 40 }}
            />
          </TouchableOpacity>
          <TouchableOpacity
            style={{
              alignSelf: 'flex-end',
              alignItems: 'center',
              backgroundColor: 'transparent',
            }}
            onPress={handleCameraType}
          >
            <MaterialCommunityIcons
              name="camera-switch"
              style={{ color: '#fff', fontSize: 40 }}
            />
          </TouchableOpacity>
        </View>
      </Camera>
    </View>
  );
};
export default (CameraScreen);
