import React, { useState } from 'react';
import {
  StyleSheet, Text, View, TextInput, TouchableHighlight,
} from 'react-native';

import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import { UPDATE_USER_PROFILE } from '../redux/store/actions/actionsTypes';
import {
  colors, appProps, font,
} from '../constants/Env';
import TopBar from '../components/TopBar.component';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.white,
    alignItems: 'center',
    justifyContent: 'center',
    width: '100%',
    height: '100%',
  },
  subContainer: {
    width: '100%',
    height: '88%',
    alignItems: 'center',
    paddingTop: 30,
  },
  inputContainer: {
    borderWidth: 1,
    borderColor: colors.orange,
    borderRadius: appProps.borderRadius,
    width: '75%',
    height: 30,
    alignItems: 'center',
    paddingLeft: appProps.textInputPadding,
    flexDirection: 'row',
    marginTop: 10,
    marginBottom: 10,
  },
  textInput: {
    marginLeft: 10,
    fontFamily: font.regular,
    fontSize: font.textSize,
    width: '94%',
  },
  titleContainer: {
    // backgroundColor: 'red',
    width: '85%',
    marginTop: 10,
    marginBottom: 20,
    borderColor: colors.orange,
    borderWidth: 1,
    borderRadius: 10,
    paddingTop: 10,
  },
  contentContainer: {
    height: '30%',
    width: '85%',
    marginTop: 10,
    marginBottom: 20,
    borderColor: colors.orange,
    borderWidth: 1,
    borderRadius: 10,
    paddingTop: 10,
  },
  classicText: {
    color: colors.orange,
    fontFamily: font.regular,
    fontSize: font.textSize,
    marginTop: '-2%',
  },
  buttonContainer: {
    backgroundColor: colors.orange,
    borderRadius: appProps.borderRadius,
    alignItems: 'center',
    justifyContent: 'center',
    width: '55%',
    height: 40,
    marginTop: 30,
  },
  buttonText: {
    color: colors.white,
    fontFamily: font.bold,
    fontSize: font.textSize,
  },
});

const EditProfileScreen = (props) => {
  const { profile, dispatch } = props;
  const state = profile;
  const [pseudo, setPseudo] = useState(state.user.pseudo);
  const [firstname, setFirstname] = useState(state.user.firstname);
  const [lastname, setLastname] = useState(state.user.lastname);
  const [bio, setBio] = useState(state.user.bio);

  const handleSubmitEditProfile = () => {
    // axios.patch(`http://${apiUrl}:8888/profile`, {
    //   pseudo,
    //   firstname,
    //   lastname,
    //   bio,
    //   id: state.user._id,
    // })
    //   .then(() => {
    //     const user = {
    //       pseudo,
    //       firstname,
    //       lastname,
    //       bio,
    //       _id: state.user._id,
    //       recettes: state.user.recettes,
    //       comis: state.user.comis,
    //       chefs: state.user.chefs,
    //     };
    const updateData = {
      pseudo, firstname, lastname, bio,
    };
    const action = { type: UPDATE_USER_PROFILE, value: { updateData, state } };
    dispatch(action);
    // })
    // .catch((error) => {
    //   if (error.response && error.response.status >= 400 && error.response.status < 500) {
    //     Alert.alert('Incident', error.response.status.toString());
    //   }
    // });
  };

  return (
    <View style={styles.container}>
      <TopBar from="newrecette" />
      <View style={styles.subContainer}>
        <View style={{ width: '80%' }}>
          <Text style={styles.classicText}>Pseudo</Text>
        </View>
        <View style={styles.titleContainer}>
          <TextInput
            style={[styles.textInput, { height: 50 }]}
            autoCapitalize="none"
            onChangeText={(title) => setPseudo(title)}
            value={pseudo}
            returnKeyType="done"
            placeholder="Titre de la recette"
            placeholderTextColor={colors.gray}
          />
        </View>
        <View style={{ width: '80%' }}>
          <Text style={styles.classicText}>Prénom</Text>
        </View>
        <View style={styles.titleContainer}>
          <TextInput
            style={[styles.textInput, { height: 50 }]}
            autoCapitalize="none"
            onChangeText={(title) => setFirstname(title)}
            value={firstname}
            returnKeyType="done"
            placeholder="Prénom"
            placeholderTextColor={colors.gray}
          />
        </View>
        <View style={{ width: '80%' }}>
          <Text style={styles.classicText}>Nom de famille</Text>
        </View>
        <View style={styles.titleContainer}>
          <TextInput
            style={[styles.textInput, { height: 50 }]}
            autoCapitalize="none"
            onChangeText={(title) => setLastname(title)}
            value={lastname}
            returnKeyType="done"
            placeholder="Mon de famille"
            placeholderTextColor={colors.gray}
          />
        </View>
        <View style={{ width: '80%' }}>
          <Text style={styles.classicText}>Bio</Text>
        </View>
        <View style={styles.contentContainer}>
          <TextInput
            multiline
            style={styles.textInput}
            autoCapitalize="none"
            onChangeText={(content) => setBio(content)}
            value={bio}
            returnKeyType="done"
            placeholder="Bio"
            placeholderTextColor={colors.gray}
          />
        </View>
        <TouchableHighlight onPress={handleSubmitEditProfile} style={styles.buttonContainer} underlayColor="#FFF">
          <View>
            <Text style={styles.buttonText}>Mettre à jour</Text>
          </View>
        </TouchableHighlight>
        <TouchableHighlight onPress={() => Actions.Login()} style={styles.buttonContainer} underlayColor="#FFF">
          <View>
            <Text style={styles.buttonText}>Deconnexion</Text>
          </View>
        </TouchableHighlight>
      </View>
    </View>
  );
};

const mapStateToProps = (state) => ({
  profile: state.profile,
});

export default connect(mapStateToProps)(EditProfileScreen);
