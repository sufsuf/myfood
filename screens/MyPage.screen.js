import React, { useState, useEffect } from 'react';
import {
  StyleSheet, Text, View, Image, TouchableHighlight, Alert, ScrollView,
} from 'react-native';

import { connect } from 'react-redux';
import axios from 'axios';
import { Actions } from 'react-native-router-flux';
import { GET_FOLLOW_USER, GET_UNFOLLOW_USER } from '../redux/store/actions/actionsTypes';

import {
  colors, appProps, font, apiUrl,
} from '../constants/Env';
import TopBar from '../components/TopBar.component';
import Footer from '../components/Footer.component';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.white,
    alignItems: 'center',
    justifyContent: 'center',
    width: '100%',
    height: '100%',
  },
  subContainer: {
    width: '92%',
    height: '78%',
    paddingTop: '7%',
  },
  topBarContainer: {
    width: '100%',
    height: '10%',
  },
  footerContainer: {
    width: '100%',
    height: '10%',
  },
  bigText: {
    color: colors.orange,
    fontFamily: font.bold,
    fontSize: font.titleSize,
  },
  classicText: {
    color: colors.orange,
    fontFamily: font.regular,
    fontSize: font.textSize,
  },
  profilePicContainer: {
    marginRight: 30,
    width: '30%',
  },
  profilePic: {
    width: 100,
    height: 100,
    borderRadius: 50,
    borderWidth: 2,
    borderColor: colors.orange,
  },
  infoTextContainer: {
    height: '40%',
    flexDirection: 'row',
  },
  numberContainer: {
    flexDirection: 'row',
    width: '100%',
    height: '6%',
  },
  topInfo: {
    flexDirection: 'row',
    width: '100%',
    height: 120,
  },
  topInfoTextContainer: {
    width: '100%',
  },
  buttonContainer: {
    backgroundColor: colors.orange,
    borderRadius: appProps.borderRadius,
    alignItems: 'center',
    justifyContent: 'center',
    width: '30%',
    height: 30,
    marginTop: 5,
    marginLeft: '12%',
  },
  buttonText: {
    color: colors.white,
    fontFamily: font.bold,
    fontSize: font.textSize,
  },
  nameBioContainer: {
    marginTop: 10,
  },
  recetteText: {
    color: 'black',
  },
  cellContainer: {
    backgroundColor: colors.orange,
    marginBottom: 10,
    height: 70,
    justifyContent: 'center',
    flexDirection: 'row',
  },
  scrollView: {
    width: '100%',
    height: '100%',
    borderTopWidth: 2,
    borderColor: colors.gray,
    flex: 1,
  },
  scrollViewContainer: {
    height: '69%',
  },
  buttonUnfollowContainer: {
    backgroundColor: colors.white,
    borderWidth: 1,
    borderColor: colors.orange,
    borderRadius: appProps.borderRadius,
    alignItems: 'center',
    justifyContent: 'center',
    width: '35%',
    height: 30,
    marginTop: 5,
    marginLeft: '12%',
  },
});

const goIcon = require('../assets/imgs/icon-go.png');

const RecetteCell = (props) => {
  const { recette } = props;
  const [newRecette, setNewRecette] = useState({});
  const [good, setGood] = useState(false);

  useEffect(() => {
    if (!good) {
      axios.get(`http://${apiUrl}:8888/recette`, {
        params: {
          id: recette,
        },
      })
        .then((response) => {
          setNewRecette(response.data);
          setGood(true);
        })
        .catch((error) => {
          if (error.response.status >= 400 && error.response.status < 500) {
            Alert.alert('Incident');
          }
        });
    }
  });
  return (
    <TouchableHighlight onPress={() => Actions.ViewRecette({ recette: newRecette })} underlayColor="#FFF">
      <View style={styles.cellContainer}>
        <View style={{ justifyContent: 'center', width: '85%' }}>
          <Text
            style={[styles.classicText, { color: colors.white, marginLeft: 15 }]}
          >
            {newRecette.title}
          </Text>
        </View>
        <View style={{ justifyContent: 'center', width: '15%' }}>
          <Image
            source={goIcon}
            style={{ width: 30, height: 30 }}
          />
        </View>
      </View>
    </TouchableHighlight>
  );
};

const MyPageScreen = (props) => {
  const { user, profile } = props;
  const [state, setState] = useState(user || profile.user);

  useEffect(() => {
    setState(profile.user);
  }, [profile]);

  const handleFollowSubmit = async () => {
    const searchData = { userId: profile.user._id, toFollowId: state._id };
    const action = { type: GET_FOLLOW_USER, value: searchData };
    await props.dispatch(action);
  };

  const handleUnFollowSubmit = async () => {
    const searchData = { userId: profile.user._id, toFollowId: state._id };
    const action = { type: GET_UNFOLLOW_USER, value: searchData };
    await props.dispatch(action);
  };

  const getHeight = () => (user ? {
    height: '88%',
  } : {
    height: '78%',
  });

  return (
    <View style={styles.container}>
      <TopBar from={state._id !== profile.user._id ? 'newrecette' : 'app'} />
      <View style={[styles.subContainer, getHeight()]}>
        <View style={styles.topInfo}>
          <View style={styles.profilePicContainer}>
            <Image
              style={styles.profilePic}
              source={{ uri: 'https://www.pngitem.com/pimgs/m/421-4212617_person-placeholder-image-transparent-hd-png-download.png' }}
            />
          </View>
          <View style={styles.topInfoTextContainer}>
            <View style={styles.infoTextContainer}>
              <Text style={[styles.bigText, { marginTop: 5 }]}>{state.pseudo}</Text>
              { state._id !== profile.user._id
              && state.comis && state.comis.indexOf(profile.user._id) === -1 && (
                <TouchableHighlight onPress={handleFollowSubmit} style={styles.buttonContainer} underlayColor="#FFF">
                  <Text style={styles.buttonText}>+ Suivre</Text>
                </TouchableHighlight>
              )}
              { state._id !== profile.user._id
              && state.comis && state.comis.indexOf(profile.user._id) > -1 && (
                <TouchableHighlight onPress={handleUnFollowSubmit} style={styles.buttonUnfollowContainer} underlayColor="#FFF">
                  <Text style={[styles.buttonText, { color: colors.orange }]}>Ne plus suivre</Text>
                </TouchableHighlight>
              )}
            </View>
            <View style={styles.numberContainer}>
              <View style={{ alignItems: 'center' }}>
                <Text style={styles.bigText}>{!state.recettes ? 0 : state.recettes.length}</Text>
                <Text style={styles.classicText}>Recettes</Text>
              </View>
              <View style={{ marginLeft: 30, alignItems: 'center' }}>
                <Text style={styles.bigText}>{!state.comis ? 0 : state.comis.length}</Text>
                <Text style={styles.classicText}>Comis</Text>
              </View>
              <View style={{ marginLeft: 30, alignItems: 'center' }}>
                <Text style={styles.bigText}>{!state.chefs ? 0 : state.chefs.length}</Text>
                <Text style={styles.classicText}>Chefs</Text>
              </View>
            </View>
          </View>
        </View>
        <View style={styles.nameBioContainer}>
          <Text style={styles.classicText}>
            {state.firstname}
            {' '}
            {state.lastname}
          </Text>
          <Text style={[styles.classicText, { marginTop: 10, marginBottom: 20 }]}>{state.bio}</Text>
        </View>
        <View style={styles.scrollViewContainer}>
          <ScrollView
            contentContainerStyle={{ flexGrow: 1 }}
            style={styles.scrollView}
          >
            {state.recettes && state.recettes.length > 0 && state.recettes.map((recette) => (
              <RecetteCell key={`${recette.id}`} recette={recette} />
            ))}
          </ScrollView>
        </View>
      </View>
      { !user && (
        <View style={styles.footerContainer}>
          <Footer currentScreen="MyPage" _id={state._id} />
        </View>
      )}
    </View>
  );
};

const mapStateToProps = (state) => ({
  profile: state.profile,
});

export default connect(mapStateToProps)(MyPageScreen);
