import React from 'react';
import {
  StyleSheet, Text, View,
} from 'react-native';

import { connect } from 'react-redux';
import { colors, appProps, font } from '../constants/Env';
import TopBar from '../components/TopBar.component';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.white,
    alignItems: 'center',
    justifyContent: 'center',
    width: '100%',
    height: '100%',
  },
  subContainer: {
    width: '100%',
    height: '88%',
    alignItems: 'center',
    paddingTop: 30,
  },
  inputContainer: {
    borderWidth: 1,
    borderColor: colors.orange,
    borderRadius: appProps.borderRadius,
    width: '75%',
    height: 30,
    alignItems: 'center',
    paddingLeft: appProps.textInputPadding,
    flexDirection: 'row',
    marginTop: 10,
    marginBottom: 10,
  },
  textInput: {
    marginLeft: 10,
    fontFamily: font.regular,
    fontSize: font.textSize,
    width: '94%',
    paddingTop: 10,
  },
  titleContainer: {
    // backgroundColor: 'red',
    height: 50,
    width: '85%',
    marginTop: 20,
    marginBottom: 40,
    borderColor: colors.orange,
    borderWidth: 1,
    borderRadius: 10,
    paddingTop: 10,
  },
  contentContainer: {
    height: '50%',
    width: '85%',
    marginTop: 20,
    marginBottom: 40,
    borderColor: colors.orange,
    borderWidth: 1,
    borderRadius: 10,
    paddingTop: 10,
  },
  classicText: {
    color: colors.orange,
    fontFamily: font.regular,
    fontSize: font.textSize,
    marginTop: '-2%',
  },
  buttonContainer: {
    backgroundColor: colors.orange,
    borderRadius: appProps.borderRadius,
    alignItems: 'center',
    justifyContent: 'center',
    width: '55%',
    height: 40,
    marginTop: 30,
  },
  buttonText: {
    color: colors.white,
    fontFamily: font.bold,
    fontSize: font.textSize,
  },
});

const ViewRecetteScreen = (props) => {
  const { recette } = props;

  return (
    <View style={styles.container}>
      <TopBar from="newrecette" />
      <View style={styles.subContainer}>
        <View style={styles.titleContainer}>
          <View style={{
            backgroundColor: 'white', marginTop: '-5%', height: 10, marginLeft: 20, width: '50%', paddingLeft: 10,
          }}
          >
            <Text style={styles.classicText}>Nom de la recette</Text>
          </View>
          <Text
            style={[styles.classicText, { marginTop: 13, marginLeft: 10, color: colors.black }]}
          >
            {recette && recette.title}
          </Text>
        </View>
        <View style={styles.contentContainer}>
          <View style={{
            backgroundColor: 'white', marginTop: '-5%', height: 10, marginLeft: 20, width: '55%', paddingLeft: 10,
          }}
          >
            <Text style={styles.classicText}>Contenu de la recette</Text>
          </View>
          <Text
            style={[styles.classicText, { marginTop: 13, marginLeft: 10, color: colors.black }]}
          >
            {recette && recette.content}
          </Text>
        </View>
      </View>
    </View>
  );
};

const mapStateToProps = (state) => ({
  profile: state.profile,
});

export default connect(mapStateToProps)(ViewRecetteScreen);
