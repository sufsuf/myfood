import React, { useState, useEffect } from 'react';
import { StyleSheet, Text, View } from 'react-native';

import { connect } from 'react-redux';

import {
  colors,
} from '../constants/Env';
import TopBar from '../components/TopBar.component';
import Footer from '../components/Footer.component';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.white,
    alignItems: 'center',
    justifyContent: 'center',
    width: '100%',
    height: '100%',
  },
  subContainer: {
    width: '100%',
    height: '78%',
    alignItems: 'center',
    paddingTop: '50%',
  },
  topBarContainer: {
    width: '100%',
    height: '10%',
  },
  footerContainer: {
    width: '100%',
    height: '10%',
  },
});

const HomeScreen = (profile) => {
  const state = profile.profile;
  const [good, setGood] = useState(false);

  useEffect(() => {
    if (!good && state.user) {
      setGood(true);
      // axios.get(`http://${apiUrl}:8888/feed`, {
      //   params: {
      //     id: state.user._id,
      //   },
      // })
      //   .then((response) => {
      //     console.log('feed', response.data);
      //   })
      //   .catch((error) => {
      //     if (error.response.status >= 400 && error.response.status < 500) {
      //       // Alert.alert('Incident')
      //     }
      //   });
    }
  }, [state]);

  return (
    <View style={styles.container}>
      <TopBar from="app" />
      <View style={styles.subContainer}>
        <Text>INAPP HOME</Text>
        <Text>{state.user ? state.user._id : ''}</Text>
      </View>
      <View style={styles.footerContainer}>
        <Footer currentScreen="Home" />
      </View>
    </View>
  );
};

const mapStateToProps = (state) => ({
  profile: state.profile,
});

export default connect(mapStateToProps)(HomeScreen);
