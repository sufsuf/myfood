export const colors = {
  white: '#FFF',
  orange: '#FF873C',
  gray: 'lightgray',
  black: '#000',
};

export const font = {
  titleSize: 30,
  textSize: 20,
  littleTextSize: 15,
  regular: 'marker-regular',
  bold: 'marker-bold',
};

export const text = {
  appName: 'MyFood',
};

export const appProps = {
  borderRadius: 30,
  textInputPadding: 20,
};

const yourIp = '192.168.1.28'; // Type your IP here
export const apiUrl = yourIp;
