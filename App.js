import * as React from 'react';

import * as Font from 'expo-font';

import { useEffect, useState } from 'react';
import Application from './components/Application';

const markerRegular = require('./assets/fonts/MarkerFeltThin_Regular.ttf');
const markerBold = require('./assets/fonts/MarkerFeltWide_Regular.ttf');

const App = () => {
  const [loaded, setLoaded] = useState(false);

  const loadFonts = async () => {
    await Font.loadAsync({
      'marker-regular': markerRegular,
      'marker-bold': markerBold,
    });
    setLoaded(true);
  };

  useEffect(() => {
    if (!loaded) {
      loadFonts().then();
    }
  }, [loaded]);

  return loaded ? (
    <Application />
    // <Provider store={Store}>
    //   <Router showNavigationBar={false}>
    //     <Scene key="root" headerLayoutPreset="center">
    //       <Scene
    //         key="Application"
    //         hideNavBar
    //         component={Application}
    //         renderBackButton={() => null}
    //         renderLeftButton={() => null}
    //           // renderRightButton={() => logoutButton}
    //         gesturesEnabled={false}
    //         initial
    //       />
    //     </Scene>
    //   </Router>
    // </Provider>
  ) : null;
};

export default App;
