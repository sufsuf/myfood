import { UPDATE_PROFILE, UPDATE_SEARCH } from '../actions/actionsTypes';

const defaultState = {
  profile: {},
};

const reducer = (state = defaultState, action) => {
  let newState;
  switch (action.type) {
    case UPDATE_PROFILE:
      newState = {
        ...state,
        profile: action.value,
      };
      return newState;
    case UPDATE_SEARCH:
      newState = {
        ...state,
        searchResult: action.value,
      };
      return newState;
    default:
      return state;
  }
};

export default reducer;
