import { applyMiddleware, compose, createStore } from 'redux';
import { createLogicMiddleware } from 'redux-logic';
import axios from 'axios';

import reducers from './reducers/reducer';
import logics from './logics/logic';

const deps = {
  httpClient: axios,
};

const logicMiddleware = createLogicMiddleware(logics, deps);

const composedMiddleware = compose(applyMiddleware(logicMiddleware));

const store = createStore(reducers, composedMiddleware);

export default store;
