import { createLogic } from 'redux-logic';
import axios from 'axios';
import { apiUrl } from '../../../constants/Env';

import {
  UPDATE_PROFILE,
  UPDATE_SEARCH,
  GET_CONNECTION,
  GET_LOGIN,
  GET_REGISTRATION,
  GET_SEARCH_USER,
  GET_FOLLOW_USER,
  GET_UNFOLLOW_USER,
  GET_USER,
  UPDATE_USER_PROFILE,
} from '../actions/actionsTypes';

const getLogin = createLogic({
  type: GET_LOGIN,
  latest: true,

  async process({ action }, dispatch, done) {
    const { pseudo, password } = action.value.user;
    await axios.post(`http://${apiUrl}:8888/signin`, {
      pseudo,
      password,
    })
      .then((response) => {
        const newAction = { type: UPDATE_PROFILE, value: { user: response.data } };
        dispatch(newAction);
        done();
      })
      .catch(() => {
        done();
      });
  },
});

const getRegistration = createLogic({
  type: GET_REGISTRATION,
  latest: true,

  async process({ action }, dispatch, done) {
    const {
      pseudo, firstname, lastname, password,
    } = action.value.user;
    axios.post(`http://${apiUrl}:8888/signup`, {
      pseudo,
      firstname,
      lastname,
      password,
    })
      .then((response) => {
        const newAction = { type: UPDATE_PROFILE, value: { user: response.data } };
        dispatch(newAction);
        done();
      })
      .catch(() => {
        done();
      });
  },
});

const searchUser = createLogic({
  type: GET_SEARCH_USER,
  latest: true,

  async process({ action }, dispatch, done) {
    const { search } = action.value;
    axios.get(`http://${apiUrl}:8888/user/search`, {
      params: {
        pseudo: search,
      },
    })
      .then((response) => {
        const result = { notFound: false, users: [] };
        if (response.data.length === 0) {
          result.notFound = true;
        } else {
          result.users = response.data;
        }
        const newAction = { type: UPDATE_SEARCH, value: { user: result } };
        dispatch(newAction);
        done();
      })
      .catch(() => {
        done();
      });
  },
});

const followUser = createLogic({
  type: GET_FOLLOW_USER,
  latest: true,

  async process({ action }, dispatch, done) {
    const { userId, toFollowId } = action.value;
    axios.post(`http://${apiUrl}:8888/user/follow`, {
      userId,
      toFollowId,
    })
      .then(() => {
        const newAction = { type: GET_USER, value: { id: toFollowId } };
        dispatch(newAction);
        done();
      })
      .catch(() => {
        done();
      });
  },
});

const unfollowUser = createLogic({
  type: GET_UNFOLLOW_USER,
  latest: true,

  async process({ action }, dispatch, done) {
    const { userId, toFollowId } = action.value;
    axios.post(`http://${apiUrl}:8888/user/unfollow`, {
      userId,
      toFollowId,
    })
      .then(() => {
        const newAction = { type: GET_USER, value: { id: toFollowId } };
        dispatch(newAction);
        done();
      })
      .catch(() => {
        done();
      });
  },
});

const getUser = createLogic({
  type: GET_USER,
  latest: true,

  async process({ action }, dispatch, done) {
    const { id } = action.value;
    axios.get(`http://${apiUrl}:8888/profile`, {
      params: {
        id,
      },
    })
      .then((response) => {
        const newAction = { type: UPDATE_PROFILE, value: { user: response.data } };
        dispatch(newAction);
        done();
      })
      .catch(() => {
        done();
      });
  },
});

const updateUserProfile = createLogic({
  type: UPDATE_USER_PROFILE,
  latest: true,

  async process({ action }, dispatch, done) {
    const {
      pseudo, firstname, lastname, bio, id, state,
    } = action.value;
    axios.patch(`http://${apiUrl}:8888/profile`, {
      pseudo,
      firstname,
      lastname,
      bio,
      id,
    })
      .then(() => {
        const user = {
          pseudo,
          firstname,
          lastname,
          bio,
          _id: state.user._id,
          recettes: state.user.recettes,
          comis: state.user.comis,
          chefs: state.user.chefs,
        };
        const connectionData = { user };
        const newAction = { type: UPDATE_PROFILE, value: connectionData };
        dispatch(newAction);
        done();
      });
  },
});

const updateProfile = createLogic({
  type: GET_CONNECTION,
  latest: true,

  async process({ action }, dispatch, done) {
    const profileData = { user: action.value.user };
    const newAction = { type: UPDATE_PROFILE, value: profileData };
    dispatch(newAction);
    done();
  },
});

export default [
  getLogin,
  getRegistration,
  getUser,
  updateProfile,
  searchUser,
  followUser,
  unfollowUser,
  updateUserProfile,
];
