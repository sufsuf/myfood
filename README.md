# myFood

myFood est une application permettant de créer / stocker et partager ses recettes de cuisines tout en ayant la possibilité de voir et suivre ce que font d'autres utilisateurs.

## Installation

Cloner les 2 projets myFood et myFoodApi:
```bash
git clone https://gitlab.com/sufsuf/myfood.git
git clone https://gitlab.com/sufsuf/myfoodapi.git
```
Le projet utilise node et son gestionnaire de paquets npm.

Installer les paquets sur les 2 projets myFood et myFoodApi:
```bash
@myFood> npm install
@myFoodApi> npm install
```

## Usage
Démarrer le serveur myFoodApi:
```bash
@myFoodApi> npm run dev
```
Récupérer l'adresse IP publique de son pc <br/>
On peut la trouver via la commande suivante:<br/>
### Linux / Mac
```bash
ifconfig
```
### Windows
```bash
ipconfig
```
La mettre dans la variable ___yourIp___ du fichier Env.js.<br/>
Démarrer l'application myFood:
```bash
@myFood> npm start
```
Le projet fonctionnant sous expo, télécharger expo sur smartphone puis scanner le QRCode généré par l'application
Pour deployer l'application if suffit de cliquer sur le bouton "Publier votre projet sur l'interface web d'expo".
Une fois deployer vous pourrez télécharger votre apk sur votre compte expo.<br/>
https://expo.io/@lds/projects/MyFood

## EsLint

Le projet suit la norme eslint airbnb, commande pour lancer le linter: 
```bash
npm run lint
```

## Licence
[myFood]()
