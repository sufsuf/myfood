import React from 'react';
import {
  StyleSheet, Text, View, TouchableHighlight,
} from 'react-native';
import { Icon } from 'react-native-elements';
import { Actions } from 'react-native-router-flux';
import { colors, font } from '../constants/Env';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    backgroundColor: colors.orange,
    alignItems: 'center',
    justifyContent: 'center',
    height: '10%',
    width: '100%',
    // borderTopLeftRadius: appProps.borderRadius,
    // borderTopRightRadius: appProps.borderRadius,
    shadowOffset: { width: 0, height: -3 },
    // shadowOpacity: 0.5,
    shadowColor: colors.black,
    shadowRadius: 3,
    elevation: 4,
  },
  iconContainer: {
    height: '100%',
    width: '33%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  text: {
    color: colors.white,
    fontSize: font.textSize,
    marginTop: 5,
    fontFamily: font.regular,
  },
});

const Footer = ({ currentPage, currentScreen }) => {
  const goToHomeScreen = () => {
    if (currentPage !== 'Home') {
      Actions.pop();
    }
  };

  const goToMyPageScreen = () => {
    if (currentScreen !== 'MyPage') {
      if (currentScreen !== 'Home') {
        Actions.pop();
      }
      Actions.MyPageScreen();
    }
  };

  const goToSearchScreen = () => {
    if (currentScreen !== 'Search') {
      if (currentScreen !== 'Home') {
        Actions.pop();
      }
      Actions.SearchScreen();
    }
  };

  return (
    <View style={styles.container}>
      <TouchableHighlight
        style={styles.iconContainer}
        onPress={goToHomeScreen}
        underlayColor="#FFF"
      >
        <View>
          <Icon
            name="home"
            type="material"
            color={colors.white}
          />
          <Text style={styles.text}>
            Accueil
          </Text>
        </View>
      </TouchableHighlight>
      <TouchableHighlight
        style={styles.iconContainer}
        onPress={goToSearchScreen}
        underlayColor="#FFF"
      >
        <View>
          <Icon
            name="search"
            type="material"
            color={colors.white}
          />
          <Text style={styles.text}>
            Rechercher
          </Text>
        </View>
      </TouchableHighlight>
      <TouchableHighlight
        style={styles.iconContainer}
        onPress={goToMyPageScreen}
        underlayColor="#FFF"
      >
        <View>
          <Icon
            name="book"
            type="material"
            color={colors.white}
          />
          <Text style={styles.text}>
            Ma page
          </Text>
        </View>
      </TouchableHighlight>
    </View>
  );
};

export default Footer;
