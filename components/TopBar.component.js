import React from 'react';
import {
  StyleSheet, Text, View, TouchableHighlight, Image,
} from 'react-native';

import { Actions } from 'react-native-router-flux';
import {
  colors, font, text, appProps,
} from '../constants/Env';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.orange,
    alignItems: 'center',
    justifyContent: 'center',
    height: '20%',
    width: '100%',
    borderBottomLeftRadius: appProps.borderRadius,
    borderBottomRightRadius: appProps.borderRadius,
    shadowOffset: { width: 1, height: 1 },
    shadowOpacity: 0.5,
    shadowColor: colors.black,
    shadowRadius: 3,
    elevation: 10,
    flexDirection: 'row',
    paddingLeft: '7%',
    paddingRight: '7%',
  },
  title: {
    fontFamily: font.bold,
    fontSize: font.titleSize,
    color: colors.white,
  },
  titleContainer: {
    height: '100%',
    paddingTop: '16%',
    width: '60%',
    alignItems: 'center',
  },
  newRecetteContainer: {
    width: '20%',
    paddingTop: '5%',
  },
  proPicContainer: {
    width: '20%',
    paddingTop: '5%',
    alignItems: 'flex-end',
  },
});

const newIcon = require('../assets/imgs/icon-new.png');
const backIcon = require('../assets/imgs/icon-back.png');

const TopBar = ({ from }) => (
  <View style={styles.container}>
    <View style={styles.newRecetteContainer}>
      { from === 'app' && (
      <TouchableHighlight
        onPress={() => { Actions.NewRecette(); }}
        style={{ width: 40, height: 40 }}
        underlayColor="#FFF"
      >
        <Image
          source={newIcon}
          style={{ width: 40, height: 40, resizeMode: 'contain' }}
        />
      </TouchableHighlight>
      )}
      { from === 'newrecette' && (
      <TouchableHighlight
        onPress={() => { Actions.pop(); }}
        style={{ width: 40, height: 40 }}
        underlayColor="#FFF"
      >
        <Image
          source={backIcon}
          style={{ width: 30, height: 30, resizeMode: 'contain' }}
        />
      </TouchableHighlight>
      )}
    </View>
    <View style={styles.titleContainer}>
      <Text style={styles.title}>{text.appName}</Text>
    </View>
    <View style={styles.proPicContainer}>
      { from === 'app' && (
      <TouchableHighlight
        onPress={() => { Actions.EditProfile(); }}
        style={{ width: 40, height: 40 }}
        underlayColor="#FFF"
      >
        <Image
          source={{ uri: 'https://www.pngitem.com/pimgs/m/421-4212617_person-placeholder-image-transparent-hd-png-download.png' }}
          style={{
            width: 40, height: 40, borderRadius: 20, borderColor: colors.white, borderWidth: 2,
          }}
        />
      </TouchableHighlight>
      )}
    </View>
  </View>
);

export default TopBar;
