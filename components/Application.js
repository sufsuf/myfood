import React from 'react';
import { Router, Scene } from 'react-native-router-flux';
import { Provider } from 'react-redux';
import Store from '../redux/store/configStore';
import HomeScreen from '../screens/Home.screen';
import SearchScreen from '../screens/Search.screen';
import MyPageScreen from '../screens/MyPage.screen';
import NewRecetteScreen from '../screens/NewRecette.screen';
import ViewRecetteScreen from '../screens/ViewRecette.screen';
import EditProfileScreen from '../screens/EditProfile.screen';
import LoginScreen from '../screens/Login.screen';
import RegisterScreen from '../screens/Register.screen';
// import App from '../App';

export default function Application() {
  return (
    <Provider store={Store}>
      <Router showNavigationBar={false}>
        <Scene key="root" headerLayoutPreset="center">
          <Scene
            key="HomeScreen"
            hideNavBar
            component={HomeScreen}
            renderBackButton={() => null}
            renderLeftButton={() => null}
            gesturesEnabled={false}
            initial
          />
          <Scene
            key="SearchScreen"
            hideNavBar
            component={SearchScreen}
            renderBackButton={() => null}
            renderLeftButton={() => null}
            gesturesEnabled={false}
          />
          <Scene
            key="MyPageScreen"
            hideNavBar
            component={MyPageScreen}
            renderBackButton={() => null}
            renderLeftButton={() => null}
            gesturesEnabled={false}
          />
          <Scene
            key="NewRecette"
            hideNavBar
            component={NewRecetteScreen}
            renderBackButton={() => null}
            renderLeftButton={() => null}
            gesturesEnabled={false}
          />
          <Scene
            key="ViewRecette"
            hideNavBar
            component={ViewRecetteScreen}
            renderBackButton={() => null}
            renderLeftButton={() => null}
            gesturesEnabled={false}
          />
          <Scene
            key="EditProfile"
            hideNavBar
            component={EditProfileScreen}
            renderBackButton={() => null}
            renderLeftButton={() => null}
            gesturesEnabled={false}
          />
          <Scene
            key="Login"
            hideNavBar
            component={LoginScreen}
            renderBackButton={() => null}
            renderLeftButton={() => null}
            gesturesEnabled={false}
            initial
          />
          <Scene
            key="Register"
            hideNavBar
            component={RegisterScreen}
            renderBackButton={() => null}
            renderLeftButton={() => null}
            gesturesEnabled={false}
          />
        </Scene>
      </Router>
    </Provider>
  );
}
